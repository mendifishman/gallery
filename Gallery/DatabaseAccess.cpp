#include "DatabaseAccess.h"
#include "Callback_funcs.h"
#include <iostream>
#include <fstream>
bool DatabaseAccess::open()
{
	
	std::string dbFileName = "galleryDB.sqlite";
	std::ifstream file(dbFileName);
	int file_exist = 1;
	int res = sqlite3_open(dbFileName.c_str(), &this->db);
	//*file_exist = _access(dbFileName.c_str(), 0);*/the function didnt work for some reason...
	if (res != SQLITE_OK)
	{
		this->db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	if (!file_exist)
	{
		const std::string create = "CREATE TABLE PICTURES(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));CREATE TABLE TAGS(USER_ID INTEGER NOT NULL, PICTURE_ID INTEGER NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS(ID), FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID));CREATE TABLE ALBUMS(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, USER_ID INTEGER NOT NULL, CREATION_DATE TEXT NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS(ID));CREATE TABLE USERS(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL);";
		char* errMessage = nullptr;
		res = sqlite3_exec(db, create.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			this->db = nullptr;
			std::cout << "Failed to open DB" << std::endl;
			return false;
		}
		std::cout << "Created a new db\n";
	}
	std::cout << "wasup\n";
	return true;
	
}

void DatabaseAccess::close()
{
	this->clear();
	sqlite3_close(this->db);
	db = nullptr;
}

void DatabaseAccess::clear()
{
	const char* sqlStatement = "DELETE FROM USERS WHERE 1=1;DELETE FROM TAGS WHERE 1=1;DELETE FROM ALBUMS WHERE 1=1;DELETE FROM PICTURES WHERE 1=1;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		this->db = nullptr;
		std::cout << "Failed to clear DB" << std::endl;
		return;
	}
	this->m_albums.clear();
	this->m_users.clear();
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	std::list<Album> list;
	std::list<Album> helper_list;
	std::string str = "SELECT * FROM ALBUMS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_albums, &list, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get all albums\n";
	}
	for (auto var : list)
	{
		helper_list.push_back(openAlbum(var.getName()));
	}
	return helper_list;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> list;
	std::list<Album> helper_list;
	std::string str = "SELECT * FROM ALBUMS WHERE USER_ID =	" + std::to_string(user.getId()) +";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_albums, &list, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get all user albums\n";
	}
	for (auto var : list)
	{
		helper_list.push_back(openAlbum(var.getName()));
	}
	return helper_list;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	std::string str = "INSERT INTO ALBUMS(NAME, USER_ID, CREATION_DATE) VALUES ('" + album.getName() + "', " + std::to_string(album.getOwnerId()) + ", '" + album.getCreationDate() + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), get_picture_id_tags, does_nothing, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to create album\n";
	}
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	std::string str = "DELETE FROM ALBUMS WHERE NAME = '" + albumName + "' AND USER_ID = " + std::to_string(userId) + ";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), does_nothing, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error in deleting album\n";
	}
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	bool is  = false;
	std::string str = "SELECT * FROM ALBUMS WHERE USER_ID =	" + std::to_string(userId) + " AND NAME = '" + albumName+  "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_does_exist, &is, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get if album exists\n";
	}
	return is;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	Album album;
	std::string str = "SELECT * FROM ALBUMS WHERE NAME = '" + albumName + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_of_album, &album, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get album\n";
	}
	std::list<Picture> pic_list;
	str = "SELECT * FROM PICTURES WHERE ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = '" + album.getName() + "' );";
    res = sqlite3_exec(db, str.c_str(), callback_get_pictures, &pic_list, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get pictures in album\n";
	}
	for (auto var : pic_list)
	{
		Picture pic = var;
		str = "SELECT * FROM TAGS WHERE PICTURE_ID = " + std::to_string(pic.getId()) + " ;";
		res = sqlite3_exec(db, str.c_str(), callback_get_taggs, &pic, &errMessage);
		if (res != SQLITE_OK) {
			std::cout << "An error happend when trying to get tags in album\n";
		}
		album.addPicture(pic);
	}
	this->album = album;
	return album;
}

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
}

void DatabaseAccess::printAlbums()
{
	std::list<Album> albums = getAlbums();
	for (const auto& album : albums)
	{
		std::cout << album;
	}
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	std::string str = "INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES ('" + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', (SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "'));";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), does_nothing, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying add picture to album by name\n";
	}
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	std::string str = "DELETE FROM PICTURES WHERE NAME = '" + pictureName + "' AND ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "' )";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), does_nothing, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying delete picture to album by name\n";
	}
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string str = "INSERT INTO TAGS VALUES((SELECT ID FROM PICTURES WHERE NAME = '" + pictureName + "' AND ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "' ))" + "," + std::to_string(userId) + ");";
	char *errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), does_nothing, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error in tagging users in picture\n";
	}
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	std::string str = "DELETE FROM TAGS WHERE PICTURE_ID = (SELECT ID FROM PICTURES WHERE NAME = '" + pictureName + "' AND ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = '" + albumName + "' )) AND USER_ID = " +  std::to_string(userId) + ";";
	char *errMessage = nullptr;
	int res = sqlite3_exec(db,str.c_str(), does_nothing, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error in untagging users in picture\n";
	}

}



void DatabaseAccess::printUsers()
{
	std::string str = "SELECT * FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_print_users, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to print users\n";
	}
}

User DatabaseAccess::getUser(int userId)
{
	std::string str = "SELECT * FROM USERS WHERE ID = " + std::to_string(userId) + " ;";
	User user(1, "1");
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_user, &user , &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get user\n";
	}
	return user;
}

void DatabaseAccess::createUser(User& user)
{
	std::string str = "INSERT INTO USERS(name) VALUES('" + user.getName() + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), does_nothing, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to create user\n";
	}
}

void DatabaseAccess::deleteUser(const User& user)
{
	std::string str = "DELETE FROM USERS WHERE ID = " + std::to_string(user.getId()) + ";DELETE FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";DELETE FROM PICTURES WHERE ALBUM_ID IN (SELECT ID FROM ALBUMS WHERE USER_ID =  " + std::to_string(user.getId()) +");DELETE FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), does_nothing, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to delete user\n";
	}
}

bool DatabaseAccess::doesUserExists(int userId)
{
	std::string str = "SELECT * FROM USERS WHERE ID = " + std::to_string(userId) +";";
	char* errMessage = nullptr;
	bool is = false;
	int res = sqlite3_exec(db, str.c_str(), callback_does_exist, &is, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get if user exists\n";
	}
	return is;
}


int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	std::string str = "SELECT COUNT (DISTINCT ID) FROM ALBUMS WHERE USER_ID =" + std::to_string(user.getId()) + ";";
	char* errMessage = nullptr;
	int count = 0;
	int res = sqlite3_exec(db, str.c_str(), callback_get_count, &count, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to count albums ownded of user\n";
	}
	return count;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	std::string str = "SELECT COUNT (ID) FROM ALBUMS WHERE (SELECT COUNT (*) FROM TAGS WHERE (SELECT ALBUM_ID FROM PICTURES WHERE ID = PICTURE_ID) = ID AND USER_ID = " + std::to_string(user.getId()) + " );";
	char* errMessage = nullptr;
	int count = 0;
	int res = sqlite3_exec(db, str.c_str(), callback_get_count, &count, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to count albums tagges of user\n";
	}
	return count;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	std::string str = "SELECT COUNT (*) FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + ";";
	char* errMessage = nullptr;
	int count = 0;
	int res = sqlite3_exec(db, str.c_str(), callback_get_count, &count, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to count taggs of user\n";
	}
	return count;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	return countTagsOfUser(user)/ countAlbumsTaggedOfUser(user)  ;
}

User DatabaseAccess::getTopTaggedUser()
{
	std::string str = "SELECT * FROM USERS WHERE (SELECT COUNT(*) FROM TAGS WHERE USER_ID = ID) = (SELECT MAX((SELECT COUNT(*) FROM TAGS WHERE USER_ID = ID)) FROM USERS);";
	User user(1, "1");
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_user, &user, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get top tagged user\n";
	}
	return user;
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	std::string str = "SELECT * FROM PICTURES WHERE (SELECT COUNT(*) FROM TAGS WHERE PICTURE_ID = ID) = (SELECT MAX((SELECT COUNT(*) FROM TAGS WHERE PICTURE_ID = ID)) FROM PICTURES);";
	Picture pic;
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, str.c_str(), callback_get_picture, &pic, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get top tagges picture\n";
	}
	return pic;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::string str = "SELECT * FROM PICTURES WHERE (SELECT COUNT(*) FROM TAGS WHERE PICTURE_ID = ID AND USER_ID = " + std::to_string(user.getId()) + ") > 0;";
	std::list<Picture> pic_list;
	std::list<Picture> pic_list_helper;
	char* errMessage;
	int res = sqlite3_exec(db, str.c_str(), callback_get_pictures, &pic_list, &errMessage);
	if (res != SQLITE_OK) {
		std::cout << "An error happend when trying to get tagged pictures of user\n";
	}
	for (auto var : pic_list)
	{
		Picture pic = var;
		str = "SELECT * FROM TAGS WHERE PICTURE_ID = " + std::to_string(pic.getId()) + " ;";
		res = sqlite3_exec(db, str.c_str(), callback_get_taggs, &pic, &errMessage);
		if (res != SQLITE_OK) {
			std::cout << "An error happend when trying to get tags in album\n";
		}
		pic_list_helper.push_back(pic);
	}
	return pic_list_helper;
}
