#include "Callback_funcs.h"
#include <stdlib.h>
#include "Album.h"
#include <list>

int callback_of_album(void* data, int argc, char** argv, char** azColName)
{
	Album* al = (Album*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "USER_ID") {
			al->setOwner(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			al->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			al->setCreationDate(argv[i]);
		}
	}
	return 0;
}

int get_picture_id_tags(void* data, int argc, char** argv, char** azColName)
{
	int* data_int = (int*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			*data_int = (atoi(argv[i]));
		}
	}
	return 0;
}

int does_nothing(void* data, int argc, char** argv, char** azColName)
{
    return 0;
}

int callback_get_albums(void* data, int argc, char** argv, char** azColName)
{
	Album al;
	std::list<Album>* list = (std::list<Album>*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "USER_ID") {
			al.setOwner(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			al.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			al.setCreationDate(argv[i]);
		}
	}
	list->push_back(al);
	return 0;
}

int callback_does_exist(void* data, int argc, char** argv, char** azColName)
{
	bool* is = (bool*)data;
	if (argc < 1)
	{
		*is = false;
	}
	else
	{
		*is = true;
	}
	return 0;
}

int callback_open_album(void* data, int argc, char** argv, char** azColName)
{
	Album* album = (Album*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "USER_ID") {
			album->setOwner(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			album->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			album->setCreationDate(argv[i]);
		}
	}
	return 0;
}
int callback_get_pictures(void* data, int argc, char** argv, char** azColName)
{
	Picture pic;
	std::list<Picture>* list = (std::list<Picture>*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			pic.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			pic.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION")
		{
			pic.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			pic.setCreationDate(argv[i]);
		}
	}
	list->push_back(pic);
	return 0;
}
int callback_print_users(void* data, int argc, char** argv, char** azColName)
{
	int id = 0;
	std::string name = "";
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			id = atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			name = argv[i];
		}
	}
	User user(id, name);
	std::cout << user << std::endl;
	return 0;
}
int callback_get_user(void* data, int argc, char** argv, char** azColName)
{
	User* user = (User*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			user->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			user->setName(argv[i]);
		}
	}
	return 0;
}
int callback_get_count(void* data, int argc, char** argv, char** azColName)
{
	int* count = (int*)data;
	*count = atoi(argv[0]);
	return 0;
}
int callback_get_picture(void* data, int argc, char** argv, char** azColName)
{
	Picture* pic = (Picture*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			pic->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			pic->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION")
		{
			pic->setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			pic->setCreationDate(argv[i]);
		}
	}
	return 0;
}
int callback_get_taggs(void* data, int argc, char** argv, char** azColName)
{
	Picture* pic = (Picture*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "USER_ID")
		{
			pic->tagUser(atoi(argv[i]));
		}
	}
	return 0;
}