#pragma once
int callback_of_album(void* data, int argc, char** argv, char** azColName);
int get_picture_id_tags(void* data, int argc, char** argv, char** azColName);
int does_nothing(void* data, int argc, char** argv, char** azColName);
int callback_get_albums(void* data, int argc, char** argv, char** azColName);
int callback_get_pictures(void* data, int argc, char** argv, char** azColName);
int callback_print_users(void* data, int argc, char** argv, char** azColName);
int callback_get_user(void* data, int argc, char** argv, char** azColName);
int callback_get_count(void* data, int argc, char** argv, char** azColName);
int callback_get_picture(void* data, int argc, char** argv, char** azColName);
int callback_get_taggs(void* data, int argc, char** argv, char** azColName);
int callback_does_exist(void* data, int argc, char** argv, char** azColName);
int callback_open_album(void* data, int argc, char** argv, char** azColName);
